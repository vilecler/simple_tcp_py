from scapy.all import *
sa = SecurityAssociation(ESP, spi=0x22, crypt_algo='AES-CBC', crypt_key='abcdefghijklmnop'.encode())
sa2 = SecurityAssociation(AH, spi=0x222, auth_algo='SHA2-384-192', auth_key=b'secret key')

f = open('text.txt', "a")
def process_packet(packet):
    packet[IPv6].show()
    e = sa.decrypt(packet[IPv6])
    e.show()
    f.write(e[Raw].load.decode())
    f.close()

def process_packet2(packet):
    packet[IPv6].show()
    e = sa2.decrypt(packet[IPv6])
    e.show()

sniff(iface="Realtek PCIe GbE Family Controller #2",count=1,filter="esp",prn=process_packet)
sniff(iface="Realtek PCIe GbE Family Controller #2",count=1,filter="ah",prn=process_packet2)