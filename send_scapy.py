from scapy.all import *

remote_ip = "2001:fc8:a72c:a:12f9:20ff:fe91:c782"

sa = SecurityAssociation(ESP, spi=0x22, crypt_algo='AES-CBC', crypt_key='abcdefghijklmnop'.encode())
sa2 = SecurityAssociation(AH, spi=0x222, auth_algo='SHA2-384-192', auth_key=b'secret key')

with open('file.txt') as f:
    s = f.read()

#Premier packet avec ESP
q = IPv6(dst=remote_ip)/UDP(sport=5001, dport=5001)/Raw(load = s)
q.show()

#On encapsule le packet dans un autre packet
p = sa.encrypt(q)
p.show()

send(p)

#Deuxième packet avec
q2 = IPv6(dst=remote_ip)/UDP(sport=5001, dport=5001)/Raw("Re coucou c'est moi 2")
q2.show()

#On encapsule le packet dans un autre packet
p2 = sa2.encrypt(q2)
p2.show()

send(p2)
