import socket
import tqdm
import os

SERVER_HOST = "2001:fc8:a72c:a::1"
SERVER_PORT = 5001

BUFFER_SIZE = 4096
SEPARATOR = "<SEPARATOR>"

#https://scapy.readthedocs.io/en/latest/api/scapy.layers.ipsec.html
#https://www.thepythoncode.com/article/send-receive-files-using-sockets-python

def main():
    print("Server is stating...")

    #s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM, 0)

    s.bind((SERVER_HOST, SERVER_PORT))
    s.listen(5)
    print(f"[*] Listening as {SERVER_HOST}:{SERVER_PORT}")

    client_socket, address = s.accept()
    print(f"[+] {address} is connected.")

    received = client_socket.recv(BUFFER_SIZE).decode()
    filename, filesize = received.split(SEPARATOR)

    filename = os.path.basename(filename)
    filesize = int(filesize)

    progress = tqdm.tqdm(range(filesize), f"Receiving {filename}", unit="B", unit_scale=True, unit_divisor=1024)
    with open(filename, "wb") as f:
        while True:
            bytes_read = client_socket.recv(BUFFER_SIZE)
            if not bytes_read:
                break

            f.write(bytes_read)
            progress.update(len(bytes_read))

    client_socket.close()
    s.close()

if __name__ == "__main__":
    main()
