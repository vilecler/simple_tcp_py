import socket
import tqdm
import os

BUFFER_SIZE = 4096
SEPARATOR = "<SEPARATOR>"

def main():
    print("Client is stating...")

    host = input("What is server IPv6?")
    port = 5001

    filename = "File.txt"

    filesize = os.path.getsize(filename)

    s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM, 0)
    s.connect((host, port))
    s.send(f"{filename}{SEPARATOR}{filesize}".encode())

    progress = tqdm.tqdm(range(filesize), f"Sending {filename}", unit="B", unit_scale=True, unit_divisor=1024)
    with open(filename, "rb") as f:
        while True:
            bytes_read = f.read(BUFFER_SIZE)
            if not bytes_read:
                break

            s.sendall(bytes_read)
            progress.update(len(bytes_read))

    s.close()



if __name__ == "__main__":
    main()
